/*
 * Unipolar stepper motor speed and direction control with Arduino
 *   and joystick
 * This is a free software with NO WARRANTY.
 * https://simple-circuit.com/
 */
 
// include Arduino stepper motor library
#include <Stepper.h>
 
// define number of steps per revolution
#define STEPS 32

//define motor stuff
#define IN1  7
#define IN2  6
#define IN3  5
#define IN4  4

#define LASER_ON 3
#define LASER_DELAY 500

/*--------------
Motor LOGIC
+---+---+----------+
| 1 | 1 | Frewheel |
| 1 | 0 | FWD      |
| 0 | 1 | BWD      |
| 0 | 0 | BRAKE    |
+---+---+----------+
----------------*/

#define MOTOR_A_pwm 3
#define MOTOR_A_dir 12
#define MOTOR_A_brk 9

#define MOTOR_B_pwm 11
#define MOTOR_B_dir 13
#define MOTOR_B_brk 8

//Motor A
short motorAtime = 0;
boolean motorAfwd = true;
byte motorApower = 0;
byte motorApowerT = 0;

//Motor B
short motorBtime = 0;
boolean motorBfwd = true;
byte motorBpower = 0;
byte motorBpowerT = 0;


int stepperTarget = 0;
int stepperSpeed = 0;

//-- Serial
#define MAX_PARAMS 8
int paramIndex = 0;
String params[MAX_PARAMS];
 
// initialize stepper library
Stepper stepper(STEPS, IN4, IN2, IN3, IN1);
 
// joystick pot output is connected to Arduino A0
#define joystick  A0
int count=0;

void stopStepper(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
}
 
void setup(){ 
  Serial.begin(115200);
  stopStepper();
  stepper.setSpeed(100);
  pinMode(LASER_ON, OUTPUT);
  digitalWrite(LASER_ON, LOW);

  //MOTOR DEFINITION STUFF
  pinMode(MOTOR_A_pwm, OUTPUT);
  pinMode(MOTOR_A_dir, OUTPUT);
  pinMode(MOTOR_A_brk, OUTPUT);
  pinMode(MOTOR_B_pwm, OUTPUT);
  pinMode(MOTOR_B_dir, OUTPUT);
  pinMode(MOTOR_B_brk, OUTPUT);
  
  for(int i=0; i < MAX_PARAMS; i++){
    params[i] = "";
  }
  
//TCCR2B = TCCR2B & B11111000 | B00000001;    // set timer 2 divisor to     1 for PWM frequency of 31372.55 Hz
//TCCR2B = TCCR2B & B11111000 | B00000010;    // set timer 2 divisor to     8 for PWM frequency of  3921.16 Hz
//TCCR2B = TCCR2B & B11111000 | B00000011;    // set timer 2 divisor to    32 for PWM frequency of   980.39 Hz
//TCCR2B = TCCR2B & B11111000 | B00000100;    // set timer 2 divisor to    64 for PWM frequency of   490.20 Hz (The DEFAULT)
TCCR2B = TCCR2B & B11111000 | B00000101;    // set timer 2 divisor to   128 for PWM frequency of   245.10 Hz
//TCCR2B = TCCR2B & B11111000 | B00000110;    // set timer 2 divisor to   256 for PWM frequency of   122.55 Hz
//TCCR2B = TCCR2B & B11111000 | B00000111;    // set timer 2 divisor to  1024 for PWM frequency of    30.64 Hz

}

void loop(){
 
  motorAtime--;
  motorBtime--;

  if(motorApower > motorApowerT){
    if(motorApowerT >= 254){
      motorApowerT = 254;
    } else {
      motorApowerT++;
    }
  } else if(motorApower < motorApowerT){
    motorApowerT--;
  }
  
  if(motorBpower > motorBpowerT){
    if(motorBpowerT >= 254){
      motorBpowerT = 254;
    } else {
      motorBpowerT++;
    }
  } else if(motorBpower < motorBpowerT){
    motorBpowerT--;
  }

  digitalWrite(MOTOR_A_dir, motorAfwd);    
  digitalWrite(MOTOR_B_dir, !motorBfwd);    
  
  if(motorAtime > 0){
    analogWrite(MOTOR_A_pwm, motorApowerT);
  } else {
    digitalWrite(MOTOR_A_pwm, LOW);
  }    
  
  if(motorBtime > 0){
    analogWrite(MOTOR_B_pwm, motorBpowerT);
  } else {
    digitalWrite(MOTOR_B_pwm, LOW);
  }
  
  /*
  if(stepperTarget > count){
    stepper.step(1);
    count++;
  } else if(stepperTarget < count){
    stepper.step(-1);
    count--;
  } else {
    stopStepper();
    Serial.print("VAL:");
    Serial.println(stepperTarget);
    Serial.print("ANG:");
    Serial.println(count*0.17578125);
    delay(1000);
  }
  */
  delay(8);
 
}


void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n' || inChar == '\0') {
      params[paramIndex] += "\0";
      
      //Handle commands referring to motors M F 255 9999 B 0 0
      if(params[0] == "M" && paramIndex >= 6){
        motorAfwd = (params[1] == "F" ? true : false);
        motorApower = params[2].toInt();
        motorAtime = params[3].toInt();
        motorBfwd = (params[4] == "F" ? true : false);
        motorBpower = params[5].toInt();
        motorBtime = params[6].toInt();
        if(motorApower > 127) motorApower = 127;
        if(motorApower < 0) motorApower = 0;
        if(motorBpower > 127) motorBpower = 127;
        if(motorBpower < 0) motorBpower = 0;
        Serial.println("OK");
      
      //TÄHÄN SAA KOSKEA <3
      } else if(params[0] == "A" && paramIndex == 1){
        stepperTarget = params[1].toInt();
        Serial.println("OK");
      } else if(params[0] == "S" && paramIndex == 1){
        stepperSpeed = params[1].toInt();
        if(stepperSpeed < 0){
          stepperSpeed = 0;
        } else if(stepperSpeed > 1500){
          stepperSpeed = 1500; 
        }
        stepper.setSpeed(stepperSpeed);
        Serial.println("OK");
      } else if(params[0] == "R" && paramIndex == 0){
        count = 0;
        stepperTarget = 0;
        Serial.println("OK");
      } else if(params[0] == "L" && paramIndex == 0){
        digitalWrite(LASER_ON ,HIGH);
        delay(LASER_DELAY);
        digitalWrite(LASER_ON, LOW);
        Serial.println("OK");
      }
      
      paramIndex = 0;
      for(int i=0; i < MAX_PARAMS; i++){
        params[i] = "";      
      }
    } else if(inChar == ' '){
      params[paramIndex] += "\0";
      paramIndex++;
      if(paramIndex >= MAX_PARAMS) paramIndex = 0;
    } else {
      params[paramIndex] += inChar;
    }
  }
}
